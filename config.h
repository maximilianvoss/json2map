#ifndef __JSON2MAP_CONFIG_H__
#define __JSON2MAP_CONFIG_H__

#define MAX_JSON_TOKENS 1<<8

#define BUFFER_LENGTH 1<<13
#define MAX_MAP_KEY_DEPTH 1<<6

#define MAP_OBJECT_SEPARATOR '.'
#define MAP_ARRAY_START '['
#define MAP_ARRAY_END ']'

#endif
